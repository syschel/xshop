#-*- coding:utf-8 -*-
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns('',

    # Examples:
    url(r'^$', 'catalog.views.index', name='home'),
    url(r'^robots.txt$', 'default.views.robotstxt', name='robotstxt'),
    url(r'^105dffd6594d.html$', 'default.views.werifyandex', name='werifyandex'),


    url(r'^page/$', 'pages.views.pagelist', name='pagelist'),
    url(r'^page/(?P<page_uri>\w+)_(?P<page_id>\d+)/$', 'pages.views.page', name='page'),


    url(r'^tinymce/', include('tinymce.urls')),

    url(r'^catalog/', include('catalog.urls')),

    url(r'^shops/$', 'catalog.views.shops', name='shops'),
    url(r'^shops/(?P<offer_uri>\w+)_(?P<offer_id>\d+)/$', 'catalog.views.shops_offer', name='shops_offer'),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^item_go/(?P<item_id>\d+)/$', 'catalog.views.item_go', name='redirect_item'),

)



# Инициализируем папку media
if settings.DEBUG:
    import debug_toolbar
    #urlpatterns += patterns('', url(r'^__debug__/', include(debug_toolbar.urls)),)
    if settings.MEDIA_ROOT:
        from django.conf.urls.static import static
        from django.contrib.staticfiles.urls import staticfiles_urlpatterns
        urlpatterns += static(settings.MEDIA_URL,
            document_root=settings.MEDIA_ROOT)
    # Эта строка опциональна и будет добавлять url'ы только при DEBUG = True
    urlpatterns += staticfiles_urlpatterns()