#-*- coding:utf-8 -*-
import os, re

from django import template

from coupons.models import CuponADVCategory

register = template.Library()

@register.inclusion_tag('menu/coupon_top_menu.html', takes_context=True)
def coupon_top_menu(context):
    # Верхнее сквозное меню купонов
    coupons_category_top = CuponADVCategory.objects.filter(show=True, parent__isnull=True).order_by('sort')
    coupons_category_sub = CuponADVCategory.objects.filter(show=True, parent__isnull=False).order_by('sort')
    return {'coupons_category_top': coupons_category_top, 'coupons_category_sub': coupons_category_sub}
