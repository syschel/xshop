#-*- coding:utf-8 -*-

from annoying.decorators import render_to
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import math, copy
from django.contrib.sitemaps import Sitemap
from functions.tools import gen_page_list
from pages.models import Pages

@render_to('pagelist.html')
def pagelist(request):
    """
    Выводим текстовую страницу
    """
    pages = Pages.objects.filter(show=True)

    pages_count = pages.count()
    page_pages = 10 # количество товаров на страницу
    page = request.GET.get('page', 1)
    pages_rows = int(math.ceil(float(pages_count) / float(page_pages))) # Сколько страниц получится
    # Небольшая функция для генерации списка циферок страниц << 1..4,5,[6],7,8..160 >>
    pages_list = gen_page_list(int(page), int(pages_rows))
    paginator = Paginator(pages, page_pages)
    try:
        pages = paginator.page(page)
    except PageNotAnInteger:
        pages = paginator.page(1)
    except EmptyPage:
        pages = paginator.page(paginator.num_pages)

    seo = {
        'title': None,
        'keywords': None,
        'description': None,
    }
    return {'pages': pages, 'seo': seo, 'item_count': pages_count, 'pages_list': pages_list}

@render_to('page.html')
def page(request, page_uri, page_id):
    """
    Выводим текстовую страницу
    """
    page = get_object_or_404(Pages, url=page_uri, pk=page_id, show=True)

    seo = {
        'title': page.title,
        'keywords': page.keywords,
        'description': page.description,
    }
    return {'page': page, 'seo': seo}

