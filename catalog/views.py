#-*- coding:utf-8 -*-
import re

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponsePermanentRedirect
from django.shortcuts import render_to_response, get_object_or_404
from annoying.decorators import render_to, ajax_request
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Count, Q
import math
import copy
import random

from functions.tools import gen_page_list
from functions.robot import seo_default
from functions.util import get_random_string
from catalog.models import Category, Item, ItemOffer, ItemVendor, ItemParams, ItemParamsName, FilterSetting, FilterSettingValues
from pages.models import Pages
from statistic.models import SubId

@render_to('catalog/index.html')
def index(request):

    ## Новостная лента
    pages = Pages.objects.filter(show=True)[:4]
    slid_pages = []
    tmp_pages = []
    num = 0
    num2 = 0
    for page in pages:
        num2 += 1
        if num == 2:
            num = 0
            slid_pages.append(copy.copy(tmp_pages))
            del tmp_pages[:]
        if num2 == 4:
            tmp_pages.append(page)
            slid_pages.append(copy.copy(tmp_pages))
        tmp_pages.append(page)
        num += 1

    category = Category.objects.filter(show=True, home=True).order_by('sort')
    #items = Item.objects.filter(show=True).order_by('?')[:12]
    items2 = Item.objects.filter(show=True).values_list("pk", flat=True).order_by()
    similar_count = len(items2)
    item_id = []
    if items2:
        while len(item_id) < 12:
            item_id.append(items2[random.randint(0, similar_count-1)])
    items = Item.objects.filter(pk__in=item_id)


    return {'slid_pages': slid_pages, 'category': category, 'items': items}

@render_to('catalog/cat_index.html')
def cat_index(request):

    return HttpResponsePermanentRedirect(reverse("home"))

@render_to('catalog/cat_name.html')
def cat_name(request, cat_uri, cat_id):
    category = get_object_or_404(Category, url=cat_uri, pk=cat_id, show=True)

    id_cat = Category.objects.filter(level__gte=category.level, left__gte=category.left,
                                     right__lte=category.right, show=True).values_list("pk", flat=True)
    id_catn = []
    for ct in id_cat:
        id_catn.append(ct)

    #args = {}
    params = {'category__in': id_catn, 'show': True}

    # Собираем фильтры
    arr_get = {}
    page_linck = ""
    q_object = ""
    if request.method == 'GET' and request.GET:
        gets = request.GET
        # Собираем офферов
        if gets.getlist('offer_val'):
            params.update({'offer__in': gets.getlist('offer_val')})
            arr_get['offer_val'] = []
            for ofvl in gets.getlist('offer_val'):
                arr_get['offer_val'].append(int(ofvl))

        # Собираем брэнды
        if gets.getlist('vendor_val'):
            params.update({'vendor__in': gets.getlist('vendor_val')})
            arr_get['vendor_val'] = []
            for ofvl in gets.getlist('vendor_val'):
                arr_get['vendor_val'].append(int(ofvl))

        # Собираем параметры
        if gets.getlist('param_val'):
            #params.update({'offer__in': gets.getlist('param_val')})
            sql_params = FilterSettingValues.objects.filter(pk__in=gets.getlist('param_val'))
            q_object = Q()
            for set_param in sql_params:
                q_object |= Q(param_item__attr=u"%s" % set_param.param_name.param_name_id, param_item__value__icontains=u"%s" % set_param.value)

            arr_get['param_val'] = []
            for ofvl in gets.getlist('param_val'):
                arr_get['param_val'].append(int(ofvl))

        # Формируем ссылку для пагинатора с фильтрами
        if arr_get:
            for key, value in arr_get.iteritems():
                for val in value:
                    page_linck += u"&%s=%s" % (key, val)

    #Собираем фильтры
    offers_cat = Item.objects.filter(category__in=id_catn).values_list('offer', flat=True).annotate(Count('offer'))
    offers = ItemOffer.objects.filter(pk__in=list(offers_cat)).order_by('name')
    brands_cat = Item.objects.filter(category__in=id_catn).values_list('vendor', flat=True).annotate(Count('vendor'))
    brands = ItemVendor.objects.filter(pk__in=list(brands_cat)).order_by('name')

    fparams = FilterSetting.objects.filter(categoryes=cat_id, show=True)

    items = Item.objects.filter(**params).order_by('-pk')
    if q_object:
        items = items.filter(q_object)

    # Пагинатор
    item_pages = 99
    paginator = Paginator(items, item_pages)
    page = request.GET.get('page', 1)
    item_count = paginator.count
    items_rows = int(math.ceil(float(item_count) / float(item_pages))) # Сколько страниц получится
    # Небольшая функция для генерации списка циферок страниц << 1..4,5,[6],7,8..160 >>
    pages_list = gen_page_list(int(page), int(items_rows))
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    #Настройки вывода мета-сео
    seo = seo_default(category, 'category')

    return {'category': category, 'items': items, 'item_count': item_count, 'pages_list': pages_list, 'seo': seo,
            'offers': offers, 'brands': brands, 'fparams': fparams, 'arr_get': arr_get, 'page_linck': page_linck}

@render_to('catalog/item.html')
def item(request, item_uri, item_id):
    """
    Карточка товара
    """
    item = get_object_or_404(Item, url=item_uri, pk=item_id)

    # Похожие товары
    similar2 = Item.objects.filter(category=item.category_id, show=True).values_list("pk", flat=True).order_by()
    similar_count = len(similar2)
    item_id = []
    if similar_count > 2:
        while len(item_id) < 4:
            item_id.append(similar2[random.randint(0, similar_count-1)])
    similars = Item.objects.filter(pk__in=item_id)

    seo = seo_default(item, 'item')

    return {'category': item.category, 'item': item, 'seo': seo, 'similars': similars}


def item_go(request, item_id):
    """
        Редирект на страницу оффера через партнёрку
    """
    item = Item.objects.filter(pk=item_id)  #.values_list("ref_url", flat=True)
    ip = request.META.get('REMOTE_ADDR', '') or request.META.get('HTTP_X_FORWARDED_FOR', '')
    if item:
        item = item[0]
        url = item.ref_url
        subid_num = u"%s_%s" % (item_id, get_random_string(12))
        pars_url = url.split("?")

        subid = SubId()
        subid.subid = subid_num
        subid.ip = ip
        subid.keywords = ""
        subid.item_id = item_id
        subid.item_name = item.name
        subid.item_url = item.get_absolute_url()
        if "LANKA_REF" in request.COOKIES:
            subid.referer = request.COOKIES.get('LANKA_REF', False).decode("utf-8")
            subid.status = SubId.TEMPL0
        else:
            subid.status = SubId.TEMPL1

        if len(pars_url) == 2:
            url = u"%s?subid=%s&%s" % (pars_url[0], subid_num, pars_url[1])
        else:
            subid.status = SubId.TEMPL2
        subid.save()
    else:
        url = request.META.get("HTTP_REFERER")
    return HttpResponseRedirect(url)


@render_to('shops/list.html')
def shops(request):
    """
        Список всех оферов
    """
    offers = ItemOffer.objects.filter(show=True).order_by('name')
    seo = seo_default(None, 'shops')

    return {'offers': offers, 'seo': seo}

@render_to('shops/offer.html')
def shops_offer(request, offer_uri, offer_id):
    one_offer = get_object_or_404(ItemOffer, url=offer_uri, pk=offer_id)
    offers = ItemOffer.objects.filter(show=True).order_by('name')
    seo = {
        'title': one_offer.title,
        'keywords': one_offer.keywords,
        'description': one_offer.description
    }
    return {'offers': offers, 'one_offer': one_offer, 'seo': seo}