#-*- coding:utf-8 -*-

import random
from catalog.models import SemanticText, SemanticTitle, Item

def cloning_text():
    """
    Достаём списки сгенерированного в синонимайзере текста и раскидываем по товарам
    """
    semtexts = SemanticText.objects.all()
    for semtext in semtexts:
        list_texts = semtext.value.split("\r")

        for num in range(0, len(list_texts)-1):
            if len(list_texts[num]) < 5:
                del list_texts[num]

        items = Item.objects.filter(show=True, category=semtext.category, subtext__isnull=True)

        for item in items:
            rand_int = random.randint(0, len(list_texts)-1)
            item.subtext = list_texts[rand_int]
            item.save()

    return True

def cloning_title():
    """
    Достаём списки сгенерированного в синонимайзере текста и раскидываем по товарам
    """
    semtexts = SemanticTitle.objects.all()
    for semtext in semtexts:
        list_texts = semtext.value.split("\r")

        for num in range(0, len(list_texts)-1):
            if len(list_texts[num]) < 5:
                del list_texts[num]

        items = Item.objects.filter(category=semtext.category, title__isnull=True).select_related('offer', 'category')
        # print items.count()
        for item in items:
            rand_int = random.randint(0, len(list_texts)-1)
            seo_title = u"%s" % list_texts[rand_int].replace("__names__", item.name).\
            replace("__prices__", u"%s" % item.price).replace("__offers__", u"%s" % item.offer).\
            replace("__categorys__", u"%s" % item.category)
            item.title = seo_title
            item.save()

    return True
