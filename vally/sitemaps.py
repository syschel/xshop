#-*- coding:utf-8 -*-

import math
from pages.models import Pages
from catalog.models import Category, Item
from django.contrib.sites.models import Site


def create_sitemaps():
    """
    Генерируем сайтмапы
    """
    print u'start sitemap'
    site = Site.objects.all()[:1]
    if site:
        site = u"http://%s" % site[0]
    else:
        raise ValueError('Not site')
    pages = Pages.objects.filter(show=True).order_by('-update')
    categoryes = Category.objects.filter(show=True).order_by('-create')
    items = Item.objects.filter(show=True).order_by('-create')

    name_files = []
    priority = '0.5'
    path = "../media/sitemap%s.xml"

    # Генерим статьи
    if pages:
        name_files.append('_pages')
        priority = '0.6'
        f = file(path % '_pages', "w")
        f.write(u'<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n')
        for page in pages:
            f.write(u"<url><loc>%s%s</loc><changefreq>weekly</changefreq><priority>%s</priority></url>" % (site, page.get_absolute_url(), priority))
        f.write(u'</urlset>')
        f.close()

    # Генерим Категории
    if categoryes:
        name_files.append('_categoryes')
        priority = '0.5'
        f = file(path % '_categoryes', "w")
        f.write(u'<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n')
        for category in categoryes:
            f.write(u"<url><loc>%s%s</loc><changefreq>weekly</changefreq><priority>%s</priority></url>" % (site, category.get_absolute_url(), priority))
        f.write(u'</urlset>')
        f.close()

    # Генерим товары
    if items:
        item_pages = 40000
        item_count = items.count()
        pages_count = int(math.ceil(float(item_count) / float(item_pages)))

        for num in range(pages_count):
            orders_min = item_pages*num
            orders_max = item_pages*(num+1)

            name_files.append('_items%s' % num)
            priority = '0.4'
            f = file(path % '_items%s' % num, "w")
            f.write(u'<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n')
            for item in items[orders_min:orders_max]:
                f.write(u"<url><loc>%s%s</loc><changefreq>weekly</changefreq><priority>%s</priority></url>" % (site, item.get_absolute_url(), priority))
            f.write(u'</urlset>')
            f.close()


    f = file(path % '', "w")
    f.write(u'<?xml version="1.0" encoding="UTF-8"?>\n<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n')
    for name_file in name_files:
        f.write(u"<sitemap><loc>%s/sitemap%s.xml</loc></sitemap>" % (site, name_file))
    f.write(u'\n</sitemapindex>')
    f.close()
    print u'stop sitemap'
    return True

