#-*- coding:utf-8 -*-


def catalog_trees():
    """
    Перестроение дерева в категориях каталога
    """

    from catalog.models import Category
    Category.objects.all().update(left=None, right=None)
    cat_null = Category.objects.filter(parent__isnull=True)

    left = 0
    right = 0
    for catnu in cat_null:
        left += 1
        catnu.left = left
        catnu.level = 1
        if catnu.category_parent.all():
            for catn2 in catnu.category_parent.all():
                left += 1
                catn2.left = left
                catn2.level = 2
                if catn2.category_parent.all():
                    for catn3 in catn2.category_parent.all():
                        left += 1
                        catn3.left = left
                        catn3.level = 3
                        if catn3.category_parent.all():
                            for catn4 in catn3.category_parent.all():
                                left += 1
                                catn4.left = left
                                catn4.level = 4
                                if catn4.category_parent.all():
                                    for catn5 in catn4.category_parent.all():
                                        left += 1
                                        catn5.left = left
                                        catn5.level = 5
                                        if catn5.category_parent.all():
                                            for catn6 in catn5.category_parent.all():
                                                left += 1
                                                catn6.left = left
                                                left += 1
                                                catn6.right = left
                                                catn6.level = 6
                                                catn6.save()
                                        left += 1
                                        catn5.right = left
                                        catn5.save()
                                left += 1
                                catn4.right = left
                                catn4.save()
                        left += 1
                        catn3.right = left
                        catn3.save()
                left += 1
                catn2.right = left
                catn2.save()
        left += 1
        catnu.right = left
        catnu.save()


    return True

def catalog_cat_show():
    """
    Отключени категорий у которых нету товаров
    """
    from django.db.models import Count
    from catalog.models import Category, Item
    items = Item.objects.filter(show=True).values_list("category", flat=True).annotate(dcount=Count('category'))
    newitem = []
    for item in items:
        newitem.append(int(item))
    Category.objects.all().update(show=False)
    Category.objects.filter(pk__in=newitem).update(show=True)
    return True


def seo_default(obj, jd):
    """
    Работаем с настройками SEO
    :param obj: Объект из БД с данными которого нужно работать
    :param jd: Тип объекта для поиска в БД шаблонов
    """
    seo = {
        'title': None,
        'keywords': None,
        'description': None
    }
    # Проверяем, заполнено СЕО в самом объекте, если да, то сразу выплёвываем результат.
    if jd == 'category' and obj.title and obj.keywords and obj.description:
        seo['title'] = obj.title
        seo['keywords'] = obj.keywords
        seo['description'] = obj.description
        return seo

    from default.models import SeoDefault
    seodef_all = SeoDefault.objects.all()
    if not seodef_all:
        return seo

    seodef = {}
    for defall in seodef_all:
        seodef[defall.get_template_seo_display()] = defall.text_seo

    if jd == 'shops':
        seo['title'] = u"%s" % seodef.get('title_shops_list')
        seo['keywords'] = u"%s" % seodef.get('keywords_shops_list')
        seo['description'] = u"%s" % seodef.get('description_shops_list')

    if jd == 'category':
        cat_name = obj.short_name if obj.short_name else obj.name
        if not obj.title and seodef.get('title_category'):
            seo['title'] = u"%s" % seodef.get('title_category').replace("__names__", cat_name)
        if not obj.keywords and seodef.get('keywords_category'):
            seo['keywords'] = u"%s" % seodef.get('keywords_category').replace("__names__", cat_name)
        if not obj.description and seodef.get('description_category'):
            seo['description'] = u"%s" % seodef.get('description_category').replace("__names__", cat_name)

    if jd == 'item':
        if not obj.title and seodef.get('title_item'):
            seo['title'] = u"%s" % seodef.get('title_item').replace("__names__", obj.name).\
                replace("__prices__", u"%s" % obj.price).replace("__offers__", u"%s" % obj.offer).\
                replace("__categorys__", u"%s" % obj.category)
        else:
            seo['title'] = obj.title
        if seodef.get('keywords_item'):
            seo['keywords'] = u"%s" % seodef.get('keywords_item').replace("__names__", obj.name).\
                replace("__prices__", u"%s" % obj.price).replace("__offers__", u"%s" % obj.offer).\
                replace("__categorys__", u"%s" % obj.category)
        if seodef.get('description_item'):
            seo['description'] = u"%s" % seodef.get('description_item').replace("__names__", obj.name).\
                replace("__prices__", u"%s" % obj.price).replace("__offers__", u"%s" % obj.offer).\
                replace("__categorys__", u"%s" % obj.category)


    return seo