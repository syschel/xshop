#!/bin/bash

SCRIPT_DIR='/home/django/oblakomody_ru/xshop/vally/'
source /home/django/env/bin/activate

date &&
cd $SCRIPT_DIR &&
python csv_admitad.py &&
date &&
service uwsgi restart &&
service nginx restart 
