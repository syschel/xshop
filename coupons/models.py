#-*- coding:utf-8 -*-

from django.db import models
from django.core.urlresolvers import reverse
from PIL import Image
from functions.files import get_file_path
import unidecode, re


#### Кампании ###
class CuponADVCategory(models.Model):
    """
    ADV Категории Кампаний
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)

    url             = models.CharField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)
    parent          = models.ForeignKey('self', verbose_name=u"Родительская категория", blank=True, null=True,
                                        help_text=u"Пустое значение, если категория первого уровня",
                                        related_name='coupon_adv_cetegory_parent')
    sort            = models.IntegerField(verbose_name=u"#Порядок сортировки", default=100, blank=True,
                                          help_text=u"Возможны отрицательные значения")

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=False)
    images          = models.ImageField(verbose_name=u'Изображение категории', upload_to=get_file_path, blank=True, null=True)

    h1              = models.CharField(verbose_name=u'Заголовок страницы H1', max_length=255, blank=True, null=True)
    title           = models.CharField(verbose_name=u'СЕО-Заголовок окна', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'СЕО-Ключевые', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'СЕО-Описание', max_length=255, blank=True, null=True)
    seo_text        = models.TextField(verbose_name=u'Описание', null=True, blank=True)

    xml_id          = models.IntegerField(blank=True, null=True)
    xml_parentid    = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.name

    def save(self, size=(150, 150)):
        # Если нет h1, воруем из названия категории
        if not self.h1:
            self.h1 = self.name
        # Если нет титлы, воруем из названия категории
        if not self.title:
            self.title = self.name
        # Если нет ключевиков, воруем из названия категории
        if not self.keywords:
            self.keywords = self.name
        # Если нет описания, воруем из названия категории
        if not self.description:
            self.description = self.name
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.replace(" ", "_")))
        super(CuponADVCategory, self).save()

        if self.images:
            # Ресайзим картинку если она пришла
            # print u"-= Алярм, картынка ресайзится при каждом сохранении категории =-"
            filename = self.images.path
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename, image.format)

    class Meta:
        verbose_name = u'1. Категории Кампаний'
        verbose_name_plural = u'1. Категории Кампаний'
        ordering = ('sort',)
        #unique_together = ('artist', 'free_dates',)


class CuponOffer(models.Model):
    """
    ADV кампании (Офер)
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=True)
    site            = models.CharField(verbose_name=u'Сайт офера', max_length=255, null=True, blank=True)
    images          = models.ImageField(verbose_name=u'Изображение офера', upload_to=get_file_path, blank=True, null=True)

    xml_id          = models.CharField(verbose_name=u'xml_id', max_length=255, null=True, blank=True)
    categories      = models.ManyToManyField(CuponADVCategory, verbose_name=u'Категория кампании', blank=True, null=True,)

    def __unicode__(self):
        return u"%s" % self.name

    def items(self):
        return self.item_offer.count()

    class Meta:
        verbose_name = u'Кампаиня'
        verbose_name_plural = u'3. Кампании'

class CuponAdvantagesOffer(models.Model):
     """
     Преимущества кампании
     """

     offer          = models.ForeignKey(CuponOffer, verbose_name=u'Кампания', related_name='advantages_offer')
     name           = models.CharField(max_length=2000, verbose_name=u'Преимущество')

     def __unicode__(self):
           return u'%s' % self.offer

     class Meta:
        verbose_name = u'Преимущества кампании'
        verbose_name_plural = u'2. Преимущества кампании'
        ordering = ['name']



#### Купоны ###
class CuponCategory(models.Model):
    """
    Список категорий купонов
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)

    url             = models.CharField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)
    parent          = models.ForeignKey('self', verbose_name=u"Родительская категория", blank=True, null=True,
                                        help_text=u"Пустое значение, если категория первого уровня")
    sort            = models.IntegerField(verbose_name=u"#Порядок сортировки", default=100, blank=True,
                                          help_text=u"Возможны отрицательные значения")

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=False)
    images          = models.ImageField(verbose_name=u'Изображение категории', upload_to=get_file_path, blank=True, null=True)

    h1              = models.CharField(verbose_name=u'Заголовок страницы H1', max_length=255, blank=False, null=True)
    title           = models.CharField(verbose_name=u'СЕО-Заголовок окна', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'СЕО-Ключевые', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'СЕО-Описание', max_length=255, blank=True, null=True)
    seo_text        = models.TextField(verbose_name=u'Описание', null=True, blank=True)

    xml_id          = models.IntegerField(blank=True, null=True)
    xml_parentid    = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.name

    def save(self, size=(150, 150)):
        # Если нет h1, воруем из названия категории
        if not self.h1:
            self.h1 = self.name
        # Если нет титлы, воруем из названия категории
        if not self.title:
            self.title = self.name
        # Если нет ключевиков, воруем из названия категории
        if not self.keywords:
            self.keywords = self.name
        # Если нет описания, воруем из названия категории
        if not self.description:
            self.description = self.name
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.replace(" ", "_")))
        super(CuponCategory, self).save()

        if self.images:
            # Ресайзим картинку если она пришла
            # print u"-= Алярм, картынка ресайзится при каждом сохранении категории =-"
            filename = self.images.path
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename, image.format)

    class Meta:
        verbose_name = u'Категории купонов'
        verbose_name_plural = u'4. Категории купонов'
        ordering = ('sort',)
        #unique_together = ('artist', 'free_dates',)

class CuponTypes(models.Model):
     """
     Тип купона
     """
     name            = models.CharField(max_length=300, verbose_name=u'Тип купона')
     xml_id          = models.IntegerField(blank=True, null=True)

     def __unicode__(self):
           return u'%s' % self.name

     class Meta:
        verbose_name = u'Тип купона'
        verbose_name_plural = u'5. Тип купона'
        ordering = ['name']

class CuponSpecies(models.Model):
     """
     Вид купона
     """
     name            = models.CharField(max_length=300, verbose_name=u'Вид купона')
     xml_id          = models.IntegerField(blank=True, null=True)

     def __unicode__(self):
           return u'%s' % self.name

     class Meta:
        verbose_name = u'Вид купона'
        verbose_name_plural = u'6. Вид купона'
        ordering = ['name']



class CuponItem(models.Model):
    """
    Купоны
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)
    sort            = models.IntegerField(verbose_name=u"#Порядок сортировки", default=100, blank=True,
                                          help_text=u"Возможны отрицательные значения")

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=False)
    short_name      = models.CharField(verbose_name=u'Короткое название', max_length=255, null=True, blank=True)
    logo            = models.CharField(verbose_name=u'logo url', max_length=255, null=True, blank=True)
    images          = models.ImageField(verbose_name=u'logo img', upload_to=get_file_path, blank=True, null=True)
    description     = models.TextField(verbose_name=u'Описание', null=True, blank=True)
    promocode       = models.CharField(verbose_name=u'promocode', max_length=255, null=True, blank=True)
    promolink       = models.CharField(verbose_name=u'promolink', max_length=255, null=True, blank=True)
    gotolink        = models.CharField(verbose_name=u'gotolink', max_length=255, null=True, blank=True)
    date_start      = models.DateTimeField(verbose_name=u"date_start", null=True, blank=True)
    date_end        = models.DateTimeField(verbose_name=u"date_end", null=True, blank=True)
    exclusive       = models.BooleanField(verbose_name=u'Отображать', default=False)
    discount        = models.CharField(verbose_name=u'discount', max_length=255, null=True, blank=True)

    categories      = models.ManyToManyField(CuponCategory, verbose_name=u'Категория купонов', blank=True, null=True, related_name='cupon_categories')
    campany         = models.ForeignKey(CuponOffer, verbose_name=u'Капания', related_name='cupon_offer')
    specie          = models.ForeignKey(CuponSpecies, verbose_name=u'Вид купона', related_name='cupon_specie')
    types           = models.ManyToManyField(CuponTypes, verbose_name=u'Тип купона', blank=True, null=True, related_name='cupon_types')

    xml_id          = models.IntegerField(blank=True, null=True)


    def __unicode__(self):
        return u"%s" % self.name


    class Meta:
        verbose_name = u'Купоны'
        verbose_name_plural = u'7. Купоны'
        ordering = ['sort']

