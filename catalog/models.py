#-*- coding:utf-8 -*-
import re
from django.db import models
from django.core.urlresolvers import reverse
from PIL import Image
from functions.files import get_file_path
from tinymce.models import HTMLField
import unidecode


class Category(models.Model):
    """
    Список категорий
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)

    home            = models.BooleanField(verbose_name=u'На главной', default=False)

    url             = models.CharField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)
    parent          = models.ForeignKey('self', verbose_name=u"Родительская категория", blank=True, null=True,
                                        help_text=u"Пустое значение, если категория первого уровня",
                                        related_name='category_parent')
    sort            = models.IntegerField(verbose_name=u"#Порядок сортировки", default=100, blank=True,
                                          help_text=u"Возможны отрицательные значения")

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=False, help_text=u"<font color='red'>НЕ ПЕРЕИМЕНОВЫВАТЬ!!! Используется в обновлении товаров</font>")
    short_name      = models.CharField(verbose_name=u'Короткое название', max_length=255, null=True, blank=True, help_text=u"Для переименовывания категории, использовать данное поле")
    images          = models.ImageField(verbose_name=u'Изображение категории', upload_to=get_file_path, blank=True, null=True)

    h1              = models.CharField(verbose_name=u'Заголовок страницы H1', max_length=255, blank=True, null=True)
    title           = models.CharField(verbose_name=u'СЕО-Заголовок окна', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'СЕО-Ключевые', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'СЕО-Описание', max_length=255, blank=True, null=True)
    seo_text        = HTMLField(verbose_name=u'Описание', null=True, blank=True)

    xml_id          = models.IntegerField(blank=True, null=True)
    xml_parentid    = models.IntegerField(blank=True, null=True)

    left            = models.IntegerField(blank=True, null=True)
    right           = models.IntegerField(blank=True, null=True)
    level           = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        if self.short_name:
            return u"%s" % self.short_name
        return u"%s" % self.name

    def titlename(self):
        if self.short_name and len(self.short_name) > 1:
            return u"%s" % self.short_name
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('cat_name', args=[self.url, self.pk])

    def save(self, size=(200, 200), *args, **kwargs):
        ## Если нет титлы, воруем из названия категории
        #if not self.title:
        #    self.title = self.name
        ## Если нет ключевиков, воруем из названия категории
        #if not self.keywords:
        #    self.keywords = self.name
        ## Если нет описания, воруем из названия категории
        #if not self.description:
        #    self.description = self.name
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.replace(" ", "_")))
        super(Category, self).save(*args, **kwargs)

        if self.images:
            # Ресайзим картинку если она пришла
            # print u"-= Алярм, картынка ресайзится при каждом сохранении категории =-"
            filename = self.images.path
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename, image.format)

    class Meta:
        verbose_name = u'Категории'
        verbose_name_plural = u'02. Категории'
        ordering = ('sort', 'name')
        #unique_together = ('name', 'parent',)
        #exclude = ['left', 'right']


class Item(models.Model):
    """
    Товары
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)
    sort            = models.IntegerField(verbose_name=u"#Порядок сортировки", default=100, blank=True,
                                          help_text=u"Возможны отрицательные значения")

    category        = models.ForeignKey('Category', verbose_name=u'Категория', related_name='item_category')
    url             = models.CharField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)
    ref_url         = models.CharField(verbose_name=u'Реф url', max_length=500, null=True, blank=False)

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=False)
    price           = models.IntegerField(verbose_name=u"Цена", default=0)
    oldprice        = models.IntegerField(verbose_name=u"Старая Цена", default=0)
    text            = models.TextField(verbose_name=u'Описание', null=True, blank=True)
    subtext         = models.TextField(verbose_name=u'Доп описание', null=True, blank=True, help_text=u"Синонимизированный текст")
    picture         = models.CharField(verbose_name=u'Картинка', max_length=255, null=True, blank=True)
    thumbnail       = models.CharField(verbose_name=u'Маленькая картинка', max_length=255, null=True, blank=True)
    available       = models.BooleanField(verbose_name=u'Наличие/Заказ', default=True, help_text=u"true: В наличии, false: Под заказ")

    xml_id          = models.CharField(verbose_name=u'xml_id', max_length=255, null=True, blank=True)
    xml_original_id = models.CharField(verbose_name=u'xml_original_id', max_length=255, null=True, blank=True)
    xml_category_id = models.CharField(verbose_name=u'xml_category_id', max_length=255, null=True, blank=True)

    offer           = models.ForeignKey('ItemOffer', verbose_name=u'Офер', related_name='item_offer')
    vendor          = models.ForeignKey('ItemVendor', verbose_name=u'Бренд', related_name='item_vendor', null=True, blank=True)

    title           = models.CharField(verbose_name=u'SEO Title', max_length=255, blank=True, null=True,
                                       help_text=u"Заполняется роботом по списку шаблонов")

    def __unicode__(self):
        return u"%s" % self.name

    def save(self, size=(150, 150)):
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.replace(" ", "_")))
        super(Item, self).save()

    def get_goto_url(self):
        return reverse('redirect_item', args=[self.pk])

    def savings(self):
        return self.oldprice - self.price

    def percent(self):
        return 100 - ((self.price * 100) / self.oldprice)

    def get_absolute_url(self):
        return reverse('item', args=[self.url, self.pk])

    class Meta:
        verbose_name = u'Товары'
        verbose_name_plural = u'03. Товары'
        ordering = ['sort', 'create']



class ItemOffer(models.Model):
    """
    Товары производитель (Офер)
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=True)
    code            = models.CharField(verbose_name=u'Код офера', max_length=255, null=True, blank=True)
    images          = models.ImageField(verbose_name=u'Изображение офера', upload_to=get_file_path, blank=True, null=True)

    payment         = HTMLField(verbose_name=u'Способы оплаты', null=True, blank=True)
    return_policy   = HTMLField(verbose_name=u'Условие доставки', null=True, blank=True)
    guarantee       = HTMLField(verbose_name=u'Гарантии', null=True, blank=True)

    url             = models.CharField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)
    min_text        = HTMLField(verbose_name=u'Краткий текст', null=True, blank=True)
    max_text        = HTMLField(verbose_name=u'Описание', null=True, blank=True)
    title           = models.CharField(verbose_name=u'СЕО-Заголовок окна', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'СЕО-Ключевые', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'СЕО-Описание', max_length=255, blank=True, null=True)

    delivery        = models.BooleanField(verbose_name=u'Доставка', default=False)

    def __unicode__(self):
        return u"%s" % self.name

    def items(self):
        return self.item_offer.count()

    def get_absolute_url(self):
        return reverse('shops_offer', args=[self.url, self.pk])

    def save(self, size=(150, 150)):
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.replace(" ", "_")))
        super(ItemOffer, self).save()

    class Meta:
        verbose_name = u'Оферы товаров'
        verbose_name_plural = u'01. Оферы товаров'


class ItemVendor(models.Model):
    """
    Товары производитель (бренд)
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)

    name            = models.CharField(verbose_name=u'Бренд', max_length=255, null=True, blank=True)
    code            = models.CharField(verbose_name=u'Код бренда', max_length=255, null=True, blank=True)

    def __unicode__(self):
        return u"%s" % self.name

    def items(self):
        return self.item_vendor.count()

    class Meta:
        verbose_name = u'Бренды товаров'
        verbose_name_plural = u'04. Бренды товаров'


class ItemPhoto(models.Model):
    """
    Товары фотографии
    """

    item            = models.ForeignKey('Item', verbose_name=u'Товар', related_name='photo_item')
    value           = models.CharField(verbose_name=u"Ссылка на картинку", max_length=255, null=True, blank=True)

    def __unicode__(self):
        return u"%s" % self.item

    class Meta:
        verbose_name = u'Фотографии товаров'
        verbose_name_plural = u'05. Фотографии товаров'


class ItemParams(models.Model):
     """
     Характеристики товара
     """
     item           = models.ForeignKey('Item', verbose_name=u"Товар", related_name='param_item')
     attr           = models.ForeignKey('ItemParamsName', verbose_name=u'Название характеристики', related_name='param_name')
     value          = models.CharField(max_length=20000, verbose_name=u'Значение', null=True, blank=True)
     sub_value      = models.CharField(max_length=500, verbose_name=u'Unit значение', help_text=u"Величина измерения: КГ, СМ", null=True, blank=True)

     def __unicode__(self):
            return u'%s: %s - %s' % (self.item, self.attr, self.value)

     class Meta:
        verbose_name = u'Характеристики'
        verbose_name_plural = u'06. Характеристики'


class ItemParamsName(models.Model):
    """
    Список характеристик
    """
    name = models.CharField(max_length=300, verbose_name=u'Название хар-тики', help_text=u"Цвет, размер, вес, ...")
    show = models.BooleanField(verbose_name=u'Отображать', default=True)

    def __unicode__(self):
        return u'%s' % self.name

    def params_count(self):
        return self.param_name.count()

    class Meta:
        verbose_name = u'Названия характеристик'
        verbose_name_plural = u'07. Названия характеристик'
        ordering = ['name']


class FilterSetting(models.Model):
    """
    Настройки фильтров
    """
    name        = models.CharField(max_length=300, verbose_name=u'Отображаемое имя', help_text=u"Выводится в категории")
    name_show   = models.CharField(max_length=300, verbose_name=u'Имя в админке', help_text=u"Выводится только в админке")
    show        = models.BooleanField(verbose_name=u'Отображать', default=True)
    categoryes  = models.ManyToManyField('Category', verbose_name=u'Категория', help_text=u"Список категорий где можно вывести этот фильтр")
    param_name  = models.ForeignKey('ItemParamsName', verbose_name=u'Характеристика из CSV')

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = u'Настройки фильтров'
        verbose_name_plural = u'08. Настройки фильтров'

class FilterSettingValues(models.Model):
    """
    Настройки фильтров список значений
    """
    param_name  = models.ForeignKey('FilterSetting', verbose_name=u'Настройки фильтра', related_name='filters_settings')
    value       = models.CharField(max_length=300, verbose_name=u'Значение')

    def __unicode__(self):
        return u"%s" % self.value

    class Meta:
        verbose_name = u'Значение фильтра'
        verbose_name_plural = u'Значения фильтра'


class SemanticText(models.Model):
    """
    Списки описаний товаров от синонимайзера
    """

    category        = models.ForeignKey('Category', verbose_name=u'Категория')
    value           = models.TextField(verbose_name=u"Список описаний", max_length=255, null=True, blank=True,
                                       help_text=u"Разделитель с новой строки.<br>")

    def __unicode__(self):
        if self.category.short_name:
            return u"%s" % self.category.short_name
        return u"%s" % self.category.name

    class Meta:
        verbose_name = u'Списоки описаний'
        verbose_name_plural = u'09. Списоки описаний'


class SemanticReviews(models.Model):
    """
    Списки отзывов от синонимайзера
    """

    category        = models.ForeignKey('Category', verbose_name=u'Категория')
    value           = models.TextField(verbose_name=u"Список отзывов", max_length=255, null=True, blank=True,
                                       help_text=u'Разделитель с новой строки.')

    def __unicode__(self):
        if self.category.short_name:
            return u"%s" % self.category.short_name
        return u"%s" % self.category.name

    class Meta:
        verbose_name = u'Списоки отзывов'
        verbose_name_plural = u'10. Списоки отзывов'


class SemanticTitle(models.Model):
    """
    Списки заголовков от синонимайзера
    """

    category        = models.ForeignKey('Category', verbose_name=u'Категория')
    value           = models.TextField(verbose_name=u"Список отзывов", max_length=255, null=True, blank=True,
                                       help_text=u"Разделитель с новой строки.<br>"
                                                 u"__names__ - название объекта(не краткое),<br>"
                                                 u"__prices__ - цена товара,<br>"
                                                 u"__offers__ - оффер товара,<br>"
                                                 u"__categorys__ - категория товара")

    def __unicode__(self):
        if self.category.short_name:
            return u"%s" % self.category.short_name
        return u"%s" % self.category.name

    class Meta:
        verbose_name = u'Списоки Title'
        verbose_name_plural = u'11. Списоки Title'


class UpdateCSV(models.Model):
    """
    Обновление файла CSV
    """
    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Включено', default=True)
    photo           = models.BooleanField(verbose_name=u'Обновлять фото', default=True)
    param           = models.BooleanField(verbose_name=u'Обновлять хар-ки', default=True)

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=True)
    urls            = models.CharField(verbose_name=u'Путь к файлу', max_length=500, null=True, blank=False)
    offer           = models.IntegerField(verbose_name=u'Оферов', default=0)
    vendor          = models.IntegerField(verbose_name=u'Брендов', default=0)
    item            = models.IntegerField(verbose_name=u'Товаров', default=0)
    category        = models.IntegerField(verbose_name=u'Категорий', default=0)
    all_times       = models.CharField(verbose_name=u'Время загрузки', default=0, max_length=255, null=True, blank=True)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = u'Обновление файла CSV'
        verbose_name_plural = u'12. Обновление файла CSV'