#!/bin/bash

SCRIPT_DIR='/var/www/tema_crzbt/data/django/lasttrends_ru/xshop/vally/'
source /var/www/tema_crzbt/data/django/env/bin/activate

date &&
cd $SCRIPT_DIR &&
python csv_admitad.py &&
date &&
service uwsgi restart &&
service mysql restart &&
service nginx restart 
