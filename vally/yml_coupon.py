#-*- coding:utf-8 -*-

import sys, os, re
import urllib, urllib2, cookielib

import xlwt
from lxml import etree, html
import unidecode
import json
from random import choice
from datetime import datetime

path = os.path.normpath(os.path.join(os.getcwd(), '..'))
sys.path.append(path)

from django.core.management import setup_environ
import settings
from vally.openfile import file_open
setup_environ(settings)


def coupon_category(dates):
    """
    Работа с категориями кампаний и купонов
    """

    tree = etree.parse(dates)
    from coupons.models import CuponADVCategory, CuponCategory

    #Категории кампаний
    adv_cat = tree.xpath('/admitad_coupons/advcampaign_categories/category')
    adv_cat_num = 0
    for node in adv_cat:
        adv_name = u"%s" % node.text
        adv_catId = int(node.get("id")) if node.get("id") else None
        adv_parentId = int(node.get("parentId")) if node.get("parentId") else None

        # Проверяем существование категории
        adv_categiry = CuponADVCategory.objects.filter(name=adv_name, xml_id=adv_catId, xml_parentid=adv_parentId)
        # Если нет то создаём
        if not adv_categiry:
            adv_categiry = CuponADVCategory(name=adv_name, xml_id=adv_catId, xml_parentid=adv_parentId)
            # Если поле парентИД в xml не пустое, значит есть родитель.
            if adv_parentId != None:
                # Ищем родителя.
                father = CuponADVCategory.objects.filter(xml_id=adv_parentId).values_list("pk", flat=True)
                #Если нашли, то присваеваем его свежей категории
                if father:
                    adv_categiry.parent_id = father[0]
            adv_categiry.save()

            adv_cat_num += 1

    #Категории купонов
    cup_cat = tree.xpath('/admitad_coupons/categories/category')
    cup_cat_num = 0
    for node in cup_cat:
        cup_name = u"%s" % node.text
        cup_catId = int(node.get("id")) if node.get("id") else None
        cup_parentId = int(node.get("parentId")) if node.get("parentId") else None

        # Проверяем существование категории
        cup_categiry = CuponCategory.objects.filter(name=cup_name, xml_id=cup_catId, xml_parentid=cup_parentId)
        # Если нет то создаём
        if not cup_categiry:
            cup_categiry = CuponCategory(name=cup_name, xml_id=cup_catId, xml_parentid=cup_parentId)
            # Если поле парентИД в xml не пустое, значит есть родитель.
            if cup_parentId != None:
                # Ищем родителя.
                father = CuponCategory.objects.filter(xml_id=cup_parentId).values_list("pk", flat=True)
                #Если нашли, то присваеваем его свежей категории
                if father:
                    cup_categiry.parent_id = father[0]
            cup_categiry.save()

            cup_cat_num += 1

    return u"adv_cat: %s, add: %s," \
           u" cup_cat: %s, add: %s" % (
        len(adv_cat),
        adv_cat_num,
        len(cup_cat),
        cup_cat_num
    )

def adv_campany(dates):
    """
    Добавляем кампании
    """

    tree = etree.parse(dates)
    from coupons.models import CuponADVCategory, CuponOffer, CuponAdvantagesOffer

    #Кампании
    adv_cam = tree.xpath('/admitad_coupons/advcampaigns/advcampaign')
    adv_cam_num = 0
    campany_list = []
    for node in adv_cam:
        campany_id          = node.get("id").strip()
        name                = node.xpath("name")[0].text.strip() if node.xpath("name") else None
        site                = node.xpath("site")[0].text.strip() if node.xpath("site") else None
        categories          = node.xpath("categories/category_id") if node.xpath("categories/category_id") else None
        campany_list.append(int(campany_id))
        campany             = CuponOffer.objects.filter(xml_id=int(campany_id), name=name, site=site)
        if not campany:
            campany = CuponOffer()
            campany.xml_id=int(campany_id)
            campany.name=name
            campany.site=site
            cat_id = []
            if categories != None:
                for categ in categories:
                    cat_id.append(int(categ.text.strip()))
            campany.save()
            campany.categories.add(*CuponADVCategory.objects.filter(xml_id__in=cat_id))

            advantages          = node.xpath("advantages/advantage") if node.xpath("advantages/advantage") else None
            if advantages != None:
                for advant in advantages:
                    CuponAdvantagesOffer.objects.get_or_create(offer=campany, name=advant.text.strip())

            adv_cam_num += 1


    # отключить все Кампании
    CuponOffer.objects.all().update(show=False)
    # включить только те что есть в списке cupon_list
    CuponOffer.objects.filter(xml_id__in=campany_list).update(show=True)

    return u"adv_camp: %s, add: %s" % (
        len(adv_cam),
        adv_cam_num
    )


def coupon_items(dates):
    """
    Добавляем кампании
    """

    tree = etree.parse(dates)
    from coupons.models import CuponOffer, CuponCategory, CuponTypes, CuponSpecies, CuponItem
    from datetime import datetime

    #Кампании
    cup_type = tree.xpath('/admitad_coupons/types/type')
    for node in cup_type:
        CuponTypes.objects.get_or_create(xml_id=int(node.get("id").strip()), name=node.text.strip())

    cup_species = tree.xpath('/admitad_coupons/species/specie')
    for node in cup_species:
        CuponSpecies.objects.get_or_create(xml_id=int(node.get("id").strip()), name=node.text.strip())

    cup_items = tree.xpath('/admitad_coupons/coupons/coupon')
    cupon_list = []
    for node in cup_items:
        xml_id          = int(node.get("id").strip())
        name            = node.xpath("name")[0].text.strip() if node.xpath("name") else None
        short_name      = node.xpath("short_name")[0].text.strip() if node.xpath("short_name") else None
        logo            = node.xpath("logo")[0].text.strip() if node.xpath("logo") else None
        description     = node.xpath("description")[0].text.strip() if node.xpath("description") else None
        promocode       = node.xpath("promocode")[0].text.strip() if node.xpath("promocode") else None
        promolink       = node.xpath("promolink")[0].text.strip() if node.xpath("promolink") else None
        gotolink        = node.xpath("gotolink")[0].text.strip() if node.xpath("gotolink") else None
        date_start      = node.xpath("date_start")[0].text.strip() if node.xpath("date_start") else None
        date_end        = node.xpath("date_end")[0].text.strip() if node.xpath("date_end") else None
        exclusive       = True if node.xpath("exclusive") and node.xpath("exclusive")[0].text.strip() == 'true' else False
        discount        = node.xpath("discount")[0].text if node.xpath("discount") else None

        advcampaign_id  = int(node.xpath("advcampaign_id")[0].text.strip()) if node.xpath("advcampaign_id") else None
        specie_id       = int(node.xpath("specie_id")[0].text.strip()) if node.xpath("specie_id") else None
        types           = node.xpath("types/type_id") if node.xpath("types/type_id") else None
        categories      = node.xpath("categories/category_id") if node.xpath("categories/category_id") else None

        coupon             = CuponItem.objects.filter(xml_id=xml_id, name=name, short_name=short_name)
        cupon_list.append(xml_id)
        try:
            if not coupon:
                coupon = CuponItem()
                coupon.xml_id = xml_id
                coupon.name = name
                coupon.short_name = short_name
                coupon.logo = logo
                coupon.description = description
                coupon.promocode = promocode
                coupon.promolink = promolink
                coupon.gotolink = gotolink
                coupon.date_start = date_start#datetime.strptime(date_start, "%Y-%m-%d %H:%M:%S")#.replace(tzinfo=timezone(settings.TIME_ZONE))
                coupon.date_end = date_end#datetime.strptime(date_end, "%Y-%m-%d %H:%M:%S")#.replace(tzinfo=timezone(settings.TIME_ZONE))
                coupon.exclusive = exclusive
                coupon.discount = discount
                campany = CuponOffer.objects.filter(xml_id=advcampaign_id)
                if campany:
                    coupon.campany = campany[0]
                specie = CuponSpecies.objects.filter(xml_id=specie_id)
                if specie:
                    coupon.specie = specie[0]

                coupon.save()

                cat_id = []
                if categories != None:
                    for categ in categories:
                        cat_id.append(int(categ.text.strip()))
                coupon.categories.add(*CuponCategory.objects.filter(xml_id__in=cat_id))

                types_id = []
                if types != None:
                    for typs in types:
                        types_id.append(int(typs.text.strip()))
                coupon.types.add(*CuponTypes.objects.filter(xml_id__in=types_id))
        except:
            print u"Error_coupon", xml_id



    # отключить все купоны
    CuponItem.objects.all().update(show=False)
    # включить только те что есть в списке cupon_list
    CuponItem.objects.filter(xml_id__in=cupon_list).update(show=True)


    return True

fal_name = "./../files/xml/admitad_coupons.xml"

print coupon_category(fal_name)
print adv_campany(fal_name)
print coupon_items(fal_name)
