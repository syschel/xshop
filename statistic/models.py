#-*- coding:utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from functions.util import get_random_string


class SubId(models.Model):
    """
    SUBid статистика
    """

    create = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    subid = models.CharField(verbose_name=u'SubID', max_length=255, null=True, blank=False)
    referer = models.TextField(verbose_name=u'Рефка', max_length=255, null=True, blank=False)
    keywords = models.TextField(verbose_name=u'Слова в запрсе', max_length=255, null=True, blank=False)
    item_id = models.CharField(verbose_name=u'ИД товара', max_length=255, null=True, blank=False)
    item_name = models.CharField(verbose_name=u'Название товара', max_length=500, null=True, blank=False)
    item_url = models.CharField(verbose_name=u'Ссылка на товар', max_length=500, null=True, blank=False)
    ip = models.IPAddressField(verbose_name=u'IP', null=True, blank=False)

    TEMPL0, TEMPL1, TEMPL2 = range(0, 3)
    TEMPLATE = (
        (TEMPL0, u'Внешний'),
        (TEMPL1, u'Прямой'),
        (TEMPL2, u'Ошибка'),
    )
    status = models.IntegerField(verbose_name=u'#Заход', max_length=1, default=TEMPL0, choices=TEMPLATE)

    def __unicode__(self):
        return u"%s" % self.subid

    class Meta:
        verbose_name = u'SubID'
        verbose_name_plural = u'SubID'
        ordering = ('-create',)