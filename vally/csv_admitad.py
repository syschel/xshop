#-*- coding:utf-8 -*-
print u"-=start file.py =-"
import sys, os
import copy
import csv
import re

import json
from StringIO import StringIO

path = os.path.normpath(os.path.join(os.getcwd(), '..'))
sys.path.append(path)

from django.core.management import setup_environ
import settings

setup_environ(settings)

from functions.robot import catalog_trees, catalog_cat_show
from catalog.models import Category, ItemOffer, ItemVendor, Item, ItemPhoto, ItemParamsName, ItemParams, UpdateCSV
from semantix_text import cloning_text, cloning_title
from sitemaps import create_sitemaps

def csv_to_list(file_url):
    """
    Вычитываем файл и загоняем товары в список
    """

    with open(file_url, 'rb') as csvfile:
        name_list = csvfile.readline().replace("\r\n", "").split(";")
        spamreader = csv.reader(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
        csv_list = []
        for rows in spamreader:
            ka = 0
            tmp_line = {}
            for row in rows:
                tmp_line[name_list[ka]] = u"%s" % row.strip().decode("utf-8")
                ka += 1
            csv_list.append(tmp_line)

    return {'name_list': name_list, 'items': csv_list}

def pre_list(list_csv, objec=None):
    """
    Работаем со списками
    :param list_csv: Список имён полей и список товаров
    """
    print u"start parse"
    #Пустые списки
    cat_level = {}
    ofer_level = {}
    vendor_level = {}
    item_id = []
    paramnsme_level = {}
    db_items = {}
    old_item = 0
    new_item = 0
    list_num = 0
    if len(list_csv) == 2 and 'name_list' in list_csv and 'category' in list_csv['name_list']:
        # Достаём все товары из БД и заносим их в список
        for itsm in Item.objects.all():
            db_items[u"%s__%s__%s" % (itsm.name, itsm.xml_id, itsm.xml_original_id)] = itsm
        # Достаём всех офферов из БД и заносим их в список
        for offer in ItemOffer.objects.all():
            ofer_level[u"offer_"+offer.code] = offer
        # Достаём все бренды из БД и заносим их в список
        for vendor in ItemVendor.objects.all():
            vendor_level[vendor.name] = vendor
        # Достаём все Категории из БД и заносим их в список
        for cats in Category.objects.all().order_by('left'):
            if cats.level == 1:
                cat_level[cats.name] = cats
            elif cats.level == 2:
                cat_level[u"%s/%s" % (cats.parent.name, cats.name)] = cats
            elif cats.level == 3:
                cat_level[u"%s/%s/%s" % (cats.parent.parent.name, cats.parent.name, cats.name)] = cats
            elif cats.level == 4:
                cat_level[u"%s/%s/%s/%s" % (cats.parent.parent.parent.name, cats.parent.parent.name, cats.parent.name, cats.name)] = cats
            elif cats.level == 5:
                cat_level[u"%s/%s/%s/%s/%s" % (cats.parent.parent.parent.parent.name, cats.parent.parent.parent.name, cats.parent.parent.name, cats.parent.name, cats.name)] = cats
            else:
                pass


        # Перебираем все товары из файла
        for item in list_csv.get('items'):
            list_num += 1

            csv_id = item.get('id').strip()
            csv_original_id = item.get('original_id').strip()
            csv_name = item.get('name').strip()
            csv_name = csv_name[:230]
            csv_price = eval(item.get('price').strip()) if item.get('price') else 0
            csv_oldprice = eval(item.get('oldprice').strip()) if item.get('oldprice') else 0
            item_id.append(csv_id)

            item_key_db = u"%s__%s__%s" % (csv_name, csv_id, csv_original_id)
            if db_items.get(item_key_db, False):
                # Товар был в БД, обновляем его цены и переходим к следущему товару в файле, пропуская код ниже continue
                items = db_items[item_key_db]
                items.price = csv_price
                items.oldprice = csv_oldprice

                if objec and objec.photo:
                    csv_picture = item.get('picture').strip()
                    csv_thumbnail = item.get('thumbnail').strip()
                    items.picture = csv_picture
                    items.thumbnail = csv_thumbnail
                    items.save()
                    csv_picture_orig = item.get('picture_orig').strip()
                    csv_picture_orig = csv_picture_orig.split(",")
                    if len(csv_picture_orig):
                        for pick in csv_picture_orig:
                            if not ItemPhoto.objects.filter(item=items, value=pick):
                                photo = ItemPhoto()
                                photo.item = items
                                photo.value = pick
                                photo.save()
                if objec and objec.param:
                    items.save()
                    csv_param = item.get('param').strip()
                    if csv_param:
                        csv_param = StringIO(csv_param)
                        csv_param = json.load(csv_param)
                        if csv_param:
                            param_text = None
                            param_attrib = None
                            param_name = None
                            param_unit = None
                            for param1 in csv_param:
                                param_text = param1.get('text')
                                param_text = param_text.split(",")
                                param_attrib = param1.get('attrib')
                                param_name = re.sub(u"[^а-яА-Я ]", "", param_attrib.get('name').strip()).replace("  ", " ")
                                param_unit = param_attrib.get('unit').strip() if param_attrib.get('unit') else None
                                if not paramnsme_level.get(param_name, False) and len(param_name):
                                    paramnsme_level[param_name] = ItemParamsName.objects.get_or_create(name=param_name)
                                for parm in param_text:
                                    if len(param_name) and len(parm) and not ItemParams.objects.filter(item=items, attr=paramnsme_level[param_name][0], value=parm, sub_value=param_unit):
                                        itparam = ItemParams()
                                        itparam.item = items
                                        itparam.attr = paramnsme_level[param_name][0]
                                        itparam.value = parm.strip()
                                        itparam.sub_value = param_unit
                                        itparam.save()
                if not objec:
                    items.save()
                old_item += 1
                continue

            ################### Работаем с категориями ###################
            #Достаём строку категории
            category = item.get('category').strip()
            # Разбили на массив через слеши
            list_category = category.split("/")

            # Если категории в списке нету
            if not cat_level.get(category, False):
                all_num = len(list_category)
                # Если категория из одного уровня в массиве Ищим в БД или создаём, заносим в список cat_level[]
                if all_num == 1:
                    categ = Category.objects.filter(name="/".join(list_category).strip())
                    if categ:
                        categ = categ[0]
                    else:
                        categ = Category()
                        categ.name = "/".join(list_category).strip()
                        categ.save()
                    cat_level["/".join(list_category)] = categ
                else:
                    # Клонируем массив и начинаем его перебирать
                    tmp_cat = copy.copy(list_category)
                    tmp_num = 0
                    for num_cat in range(len(list_category)):
                        # Если всего одна категория
                        if tmp_num < 1:
                            # Нет категории в списке, создаём её
                            if not cat_level.get(tmp_cat[num_cat], False):
                                categ = Category.objects.filter(name=tmp_cat[num_cat].strip())
                                if categ:
                                    categ = categ[0]
                                else:
                                    categ = Category()
                                    categ.name = tmp_cat[num_cat].strip()
                                    categ.save()
                                cat_level[tmp_cat[num_cat]] = categ
                        # Если больше одной категории
                        else:
                            # Проверяем есть ли в списке эа категория
                            if not cat_level.get("/".join(tmp_cat[0:num_cat+1]), False):
                                tmp_cat_new = copy.copy(tmp_cat[0:num_cat+1])
                                 # Есть ли родитель в списке (он обязан там быть иначе писец скрипту :) )
                                if cat_level.get("/".join(tmp_cat_new[:-1]), False):
                                    parent_cat = cat_level.get("/".join(tmp_cat_new[:-1]))

                                    categ = Category.objects.filter(name=tmp_cat_new[-1].strip(), parent=parent_cat)
                                    if categ:
                                        categ = categ[0]
                                    else:
                                        categ = Category()
                                        categ.name = tmp_cat_new[-1].strip()
                                        categ.parent = parent_cat
                                        categ.save()

                                    cat_level["/".join(tmp_cat_new)] = categ
                        tmp_num += 1
            ################### Работаем с категориями конец ###################


            ################### Работаем с Оферами ###################
            offer_name = item.get('advcampaign_name').strip()
            offer_code = item.get('advcampaign_id').strip()
            if not ofer_level.get(u"offer_"+offer_code, False):
                offers = ItemOffer.objects.filter(code=offer_code) #, name=offer_name
                if offers:
                    offers = offers[0]
                else:
                    offers = ItemOffer()
                    offers.code = offer_code
                    offers.name = offer_name
                    offers.save()
                ofer_level[u"offer_"+offer_code] = offers
            ################### Работаем с Оферами конец ###################

            ################### Работаем с Брендами ###################
            vendor_name = item.get('vendor').strip()
            vendor_code = item.get('vendorCode').strip()

            if not vendor_level.get(vendor_name, False) and vendor_name and len(vendor_name) > 1:
                vendors = ItemVendor.objects.filter(code=vendor_code, name=vendor_name)
                if vendors:
                    vendors = vendors[0]
                else:
                    vendors = ItemVendor()
                    vendors.code = vendor_code
                    vendors.name = vendor_name
                    vendors.save()
                vendor_level[vendor_name] = vendors
            ################### Работаем с Брендами конец ###################

            ################### Работаем с товарами ###################
            #"""



            csv_available = True if item.get("available").strip() == "true" else False


            csv_ref_url = item.get('url').strip()
            csv_picture = item.get('picture').strip()
            csv_thumbnail = item.get('thumbnail').strip()

            csv_description = item.get('description').strip()
            csv_model = item.get('model').strip()
            csv_typePrefix = item.get('typePrefix').strip()
            csv_picture_orig = item.get('picture_orig').strip()
            csv_picture_orig = csv_picture_orig.split(",")
            csv_param = item.get('param').strip()
            if csv_param:
                csv_param = StringIO(csv_param)
                csv_param = json.load(csv_param)

            ### Работаем с БД товаров ###
            item = Item()
            item.price = csv_price
            item.oldprice = csv_oldprice
            item.name = csv_name
            item.xml_id = csv_id
            item.xml_original_id = csv_original_id
            item.text = csv_description
            item.picture = csv_picture
            item.thumbnail = csv_thumbnail
            item.available = csv_available
            item.ref_url = csv_ref_url
            item.category = cat_level[category]
            item.offer = ofer_level[u"offer_"+offer_code]
            if vendor_name and len(vendor_name) > 1:
                item.vendor = vendor_level[vendor_name]

            #print u"%s" % csv_ref_url
            item.save()

            new_item += 1
            if len(csv_picture_orig):
                for pick in csv_picture_orig:
                    if len(pick) > 250:
                        continue
                    if not ItemPhoto.objects.filter(item=item, value=pick):
                        photo = ItemPhoto()
                        photo.item = item
                        photo.value = pick
                        photo.save()
            if csv_param:
                param_text = None
                param_attrib = None
                param_name = None
                param_unit = None
                for param1 in csv_param:
                    param_text = param1.get('text')
                    param_text = param_text.split(",")
                    param_attrib = param1.get('attrib')
                    param_name = re.sub(u"[^а-яА-Я ]", "", param_attrib.get('name').strip()).replace("  ", " ")
                    param_unit = param_attrib.get('unit').strip() if param_attrib.get('unit') else None
                    if not paramnsme_level.get(param_name, False) and len(param_name):
                        paramnsme_level[param_name] = ItemParamsName.objects.get_or_create(name=param_name)
                    for parm in param_text:
                        if len(param_name) and len(parm) and not ItemParams.objects.filter(item=item, attr=paramnsme_level[param_name][0], value=parm, sub_value=param_unit):
                            itparam = ItemParams()
                            itparam.item = item
                            itparam.attr = paramnsme_level[param_name][0]
                            itparam.value = parm.strip()
                            itparam.sub_value = param_unit
                            itparam.save()
            ### Работаем с БД товаров Конец ###
            #"""
            ################### Работаем с товарами конец ###################


        # отключить все товары
        Item.objects.all().update(show=False)
        # включить только те что есть в списке item_id
        Item.objects.filter(xml_id__in=item_id).update(show=True)

        print
        print u"All catt: %s" % len(cat_level)
        print u"all offer: %s" % len(ofer_level)
        print u"all brend: %s" % len(vendor_level)
        print u"all item: %s" % len(item_id)
        print u"new item: %s" % new_item
        print u"upd item: %s" % old_item
        print u"all line: %s" % len(list_csv.get('items'))
        print
        print


    return {'offer': len(ofer_level), 'vendor': len(vendor_level), 'item': len(item_id), 'category': len(cat_level)}




import time
import urllib

csv_files = UpdateCSV.objects.filter(show=True)

for csv_file in csv_files:
    file_url = "./../files/csv/admitad_items.csv"
    # Скачиваем файл
    download_start = time.clock()

    urllib.urlretrieve(csv_file.urls, file_url)
    download_stop = time.clock()

    #чистка фото или параметров
    if csv_file.photo:
        ItemPhoto.objects.all().delete()
    if csv_file.param:
        ItemParams.objects.all().delete()
        ItemParamsName.objects.all().delete()
    # Парсим файл
    file_start = time.clock()
    list_csv = csv_to_list(file_url)
    file_stop = time.clock()
    # Обновляем БД товаров
    item_start = time.clock()
    pre_list(list_csv, csv_file)
    item_stop = time.clock()
    # Перестраиваем дерево категорий
    trees_start = time.clock()
    catalog_trees()
    #catalog_cat_show()
    trees_stop = time.clock()
    # Добавляем новым товарам сгенерированные описания
    cloning_text_start = time.clock()
    cloning_text()
    cloning_title()
    #catalog_cat_show()
    cloning_text_stop = time.clock()
    
    print
    print u"Download: %s sec." % (download_stop - download_start)
    print u"file: %s sec." % (file_stop - file_start)
    print u"items: %s sec." % (item_stop - item_start)
    print u"catalog_trees: %s sec." % (trees_stop - trees_start)
    print u"cloning_text: %s sec." % (cloning_text_stop - cloning_text_start)
    print u"all time: %s sec." % (cloning_text_stop - download_start)
    print

    csv_file.all_times = (cloning_text_stop - download_start)
    csv_file.save()

create_sitemaps()
	
# Декодируем файл в utf-9
#print encode_files([file_url], f_enc="utf-8")


print u"-=stop file.py =-"


## Удаление всего из БД по товарам ##
#ItemPhoto.objects.all().delete()
#ItemVendor.objects.all().delete()
#ItemParamsName.objects.all().delete()
#ItemParams.objects.all().delete()
#Item.objects.all().delete()
#Category.objects.all().delete()
#ItemOffer.objects.all().delete()
