# -*- coding: utf-8 -*-
import os
import uuid
import codecs

def get_file_path(instance, filename):
    """
    Изменяем имя файла и уникализируем путь до него
    :param instance: Объект из которого была запущена функция
    :param filename: Текущее имя файла. Нужно только для расширения
    :return: Имя файла с путём до директории
    """
    # Переименовываем файл
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)

    # Определяем от какого класса у нас загрузка файла
    name = instance.__class__.__name__

    return os.path.join("upload/class/" + name, filename)

def encode_files(files_list, f_enc="windows-1251", t_enc="utf-8"):
    """
    Перекодируем список файлов
    f_enc - из кодировки
    t_enc - в кодировку
    """

    file_postfix = t_enc.replace("-", "")

    for file in files_list:
        # перекодируем файл
        BLOCKSIZE = 1048576
        with codecs.open(file, "r", f_enc) as sourceFile:
            with codecs.open(file.replace(".csv", "_%s.csv" % file_postfix), "w", t_enc) as targetFile:
                while True:
                    contents = sourceFile.read()
                    if not contents:
                        break
                    targetFile.write(contents)
    #return True