#-*- coding:utf-8 -*-
import urllib
from django.contrib.sites.models import Site


class RefererStoreMiddleware(object):
    """Get referring page and store it to the Cookie"""

    def process_request(self, request):
        request.LANKA_REF = None
        if "LANKA_REF" in request.COOKIES:
            try:
                request.LANKA_REF = request.COOKIES["LANKA_REF"].decode("utf-8")
            except UnicodeDecodeError:
                pass
            return
        referer_url = request.META.get("HTTP_REFERER", None)

        if referer_url is not None and referer_url != "" and \
                not self._is_local_domain(referer_url):
            try:
                request.LANKA_REF = urllib.unquote_plus(referer_url).decode("utf-8")
            except UnicodeDecodeError:
                pass

    def process_response(self, request, response):
        if hasattr(request, "LANKA_REF") and request.LANKA_REF is not None and \
                request.LANKA_REF != "":
            try:
                response.set_cookie("LANKA_REF", request.LANKA_REF.encode("utf-8"),
                        max_age=365*24*3600)
            except UnicodeDecodeError:
                pass
        return response

    def _is_local_domain(self, url):
        for domain in Site.objects.values_list('domain', flat=True).filter():
            if url.find(domain) > 0:
                return True
        return False