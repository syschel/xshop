#-*- coding:utf-8 -*-
from django.contrib import admin
from pages.models import Pages


class PagesAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'show', 'create', 'update')
    list_display_links = ('name',)
    list_filter = ('show',)
    search_fields = ['name']

    fieldsets = (
        (u"SEO", {'fields': ('title', 'keywords', 'description', 'url')}),
        (u"Общее", {'fields': ('name', 'images', 'min_text', 'text')}),
        (u"Настройки", {'fields': ('show',)}),
    )



admin.site.register(Pages, PagesAdmin)

