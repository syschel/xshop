#-*- coding:utf-8 -*-
import sys
import os
path = os.path.normpath(os.path.join(os.getcwd(), '..'))
sys.path.append(path)
from django.core.management import setup_environ
import settings
setup_environ(settings)

print u"-=start file.py =-"
from semantix_text import cloning_title
cloning_title()
print u"-=stop file.py =-"
