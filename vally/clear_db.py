#-*- coding:utf-8 -*-
import sys
import os

path = os.path.normpath(os.path.join(os.getcwd(), '..'))
sys.path.append(path)

from django.core.management import setup_environ
import settings

setup_environ(settings)

from functions.robot import catalog_trees, catalog_cat_show
from catalog.models import Category, ItemOffer, ItemVendor, Item, ItemPhoto, ItemParamsName, ItemParams, UpdateCSV
from semantix_text import cloning_text

## Удаление всего из БД по товарам ##
ItemPhoto.objects.all().delete()
print u"удалены фотки"
ItemVendor.objects.all().delete()
print u"удалены бренды"
ItemParamsName.objects.all().delete()
print u"удалены значения характеристик"
ItemParams.objects.all().delete()
print u"удалены названия характеристик"
Item.objects.all().delete()
print u"удалены товары"
#Category.objects.all().delete()
ItemOffer.objects.all().delete()
print u"удалены оферы"