# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns


urlpatterns = patterns('catalog.views',
    url(r'^$', 'cat_index', name='cat_index'),
    url(r'^cat_(?P<cat_uri>\w+)_(?P<cat_id>\d+)/$', 'cat_name', name='cat_name'),
    url(r'^item_(?P<item_uri>\w+)_(?P<item_id>\d+)/$', 'item', name='item'),
)