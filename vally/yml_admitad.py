#-*- coding:utf-8 -*-

import sys, os, re

from lxml import etree
path = os.path.normpath(os.path.join(os.getcwd(), '..'))
sys.path.append(path)

from django.core.management import setup_environ
import settings
from vally.openfile import file_open
from functions.robot import catalog_trees
setup_environ(settings)




def admitad_category(dates):
    """
    Обновляем категории адмитейда
    """

    tree = etree.parse(dates)
    xml = tree.xpath('/yml_catalog/shop/categories/category')
    from catalog.models import Category

    aa = 0
    for node in xml:
        name = u"%s" % node.text
        catId = int(node.get("id")) if node.get("id") else None
        parentId = int(node.get("parentId")) if node.get("parentId") else None

        # Проверяем существование категории
        categiry = Category.objects.filter(name=name, xml_id=catId, xml_parentid=parentId)
        # Если нет то создаём
        if not categiry:
            categiry = Category(name=name, xml_id=catId, xml_parentid=parentId)
            aa += 1
        else:
            categiry = categiry[0]

        # Если поле парентИД в xml не пустое, значит есть родитель.
        if parentId != None:
            # Ищем родителя.
            father = Category.objects.filter(xml_id=parentId).values_list("pk", flat=True)
            #Если нашли, то присваеваем его свежей категории
            if father:
                categiry.parent_id = father[0]
        categiry.save()

    # Запускаем перестроение делевьев
    print u"Пересобираем деревья"
    catalog_trees()
    return u"Категорий в файле: %s, добавленно: %s" % (len(xml), aa)



def admitad_items(dates):
    """
    Работаем с товарными группами
    """

    tree = etree.parse(dates)
    xml = tree.xpath('/yml_catalog/shop/offers/offer')
    from catalog.models import Category, Item, ItemOffer, ItemVendor, ItemPhoto, ItemParams, ItemParamsName


    aa = 0
    err1 = 0
    list_item = []
    item_id = []
    for node in xml:
        aa += 1
        #try:
        item_Id             = node.get("id") if node.get("id") else None
        original_id         = node.get("original_id") if node.get("original_id") else None
        # true =наличие, false = под заказ
        available           = True if node.get("available") == "true" else False

        name                = node.xpath("name")[0].text.strip() if node.xpath("name") else False
        ref_url             = node.xpath("url")[0].text.strip() if node.xpath("url") else False
        picture             = node.xpath("picture")[0].text.strip() if node.xpath("picture") else False
        thumbnail           = node.xpath("thumbnail")[0].text.strip() if node.xpath("thumbnail") else False
        price               = node.xpath("price")[0].text.strip() if node.xpath("price") else "0"
        oldprice            = node.xpath("oldprice")[0].text.strip() if node.xpath("oldprice") else "0"
        description         = node.xpath("description")[0].text.strip() if node.xpath("description") else None
        model               = node.xpath("model")[0].text.strip() if node.xpath("model") else False
        typePrefix          = node.xpath("typePrefix")[0].text.strip() if node.xpath("typePrefix") else False
        advcampaign_name    = node.xpath("advcampaign_name")[0].text.strip() if node.xpath("advcampaign_name") else False
        advcampaign_id      = node.xpath("advcampaign_id")[0].text.strip() if node.xpath("advcampaign_id") else False

        vendor              = node.xpath("vendor")[0].text.strip() if node.xpath("vendor") else False
        vendorCode          = node.xpath("vendorCode")[0].text.strip() if node.xpath("vendorCode") else None
        categoryId          = node.xpath("categoryId")[0].text.strip() if node.xpath("categoryId") else None

        if categoryId == None:
            print "error categoryId_None", item_Id

        params               = node.xpath("param")
        # Перебераем характеристики
        list_param = []
        for param in params:
            param_name = param.get("name")
            param_text = param.text.strip() if param.text else None
            param_unit = param.get("unit")
            list_param.append({
                'param_name': param_name,
                'param_text': param_text,
                'param_unit': param_unit,
            })

            #print param_name, ":", param_text, ",", param_unit

        # Перебераем картинки оффера
        pictures_orig        = node.xpath("picture_orig")
        list_pictures = []
        for pictu in pictures_orig:
            picture_orig = pictu.text.strip()
            list_pictures.append(picture_orig)
            #print picture_orig

        list_item.append({
            'item_Id': item_Id,
            'obj': {
                'item_Id': item_Id,
                'original_id': original_id,
                'available': available,
                'name': name,
                'ref_url': ref_url,
                'picture': picture,
                'thumbnail': thumbnail,
                'price': eval(price),
                'oldprice': eval(oldprice),
                'description': description,
                'model': model,
                'typePrefix': typePrefix,
                'advcampaign_name': advcampaign_name,
                'advcampaign_id': advcampaign_id,
                'vendor': vendor,
                'vendorCode': vendorCode,
                'categoryId': categoryId,
                'params': list_param,
                'pictures_orig': list_pictures,
            }
        })
        item_id.append(item_Id)

        #except:
        #    err1 += 1
    print u"Товаров в файле: %s" % aa

    #Перебераем получившийся список товаров
    object = {}
    if list_item and len(list_item):
        for val in list_item:
            #Собираем список категорий ссылками на объекты в БД по текущей выгрузке
            if val['obj'].get('categoryId') and not 'category_%s' % val['obj'].get('categoryId') in object:
                print val['obj'].get('item_Id'), val['obj'].get('categoryId')
                object['category_%s' % val['obj'].get('categoryId')] = Category.objects.get(xml_id=val['obj'].get('categoryId'))

            #Собираем список офферов ссылками на объекты в БД по текущей выгрузке
            if val['obj'].get('advcampaign_id') and not 'offer_%s' % val['obj'].get('advcampaign_id') in object:
                offer = ItemOffer.objects.get_or_create(code=val['obj'].get('advcampaign_id'), name=val['obj'].get('advcampaign_name'))
                object['offer_%s' % val['obj'].get('advcampaign_id')] = offer

            #Собираем список бренды ссылками на объекты в БД по текущей выгрузке
            if val['obj'].get('vendor'):
                #print val['obj'].get('vendor')
                vendorrep = re.sub(u"[^a-zA-Zа-я-А-ЯёЁ0-9]", "", val['obj'].get('vendor').lower())
                if not 'vendor_%s' % vendorrep in object:
                    vendor = ItemVendor.objects.get_or_create(code=val['obj'].get('vendorCode'), name=val['obj'].get('vendor'))
                    object['vendor_%s' % vendorrep] = vendor

        for val in list_item:
            item = Item.objects.filter(xml_id=val.get('item_Id'))
            if item:
                item = item[0]
            else:
                item = Item(xml_id=val.get('item_Id'))
            item.name = val['obj'].get('name')
            item.text = val['obj'].get('description')
            item.picture = val['obj'].get('picture')
            item.thumbnail = val['obj'].get('thumbnail')
            item.available = val['obj'].get('available')
            item.xml_original_id = val['obj'].get('original_id')
            item.xml_category_id = val['obj'].get('categoryId')
            #uri_url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(val['obj'].get('name')).replace(" ", "_"))
            #item.url = uri_url
            item.ref_url = val['obj'].get('ref_url')

            item.category = object['category_%s' % val['obj'].get('categoryId')]
            item.offer = object['offer_%s' % val['obj'].get('advcampaign_id')][0]
            if val['obj'].get('vendor'):
                vendorrep = re.sub(u"[^a-zA-Zа-я-А-ЯёЁ0-9]", "", val['obj'].get('vendor').lower())
                item.vendor = object['vendor_%s' % vendorrep][0]

            item.price = val['obj'].get('price')
            item.oldprice = val['obj'].get('oldprice')

            item.save()

            pictures_orig = val['obj'].get('pictures_orig')
            for pick in pictures_orig:
                photo = ItemPhoto.objects.filter(item=item, value=pick)
                if not photo:
                    photo = ItemPhoto()
                    photo.item = item
                    photo.value = pick
                    photo.save()

            params = val['obj'].get('params')
            for param in params:
                param_name = param.get('param_name').lower()
                param_text = param.get('param_text').lower()
                param_unit = param.get('param_unit').lower() if param.get('param_unit') else None
                attr = ItemParamsName.objects.filter(name=param_name)
                if attr:
                    attr = attr[0]
                else:
                    attr = ItemParamsName()
                    attr.name = param_name
                    attr.save()
                ItemParams.objects.get_or_create(item=item, attr=attr, value=param_text, sub_value=param_unit)



            #print val.get('item_Id'), len(list_item), item




        # отключить все товары
        Item.objects.all().update(show=False)
        # включить только те что есть в списке item_id
        Item.objects.filter(xml_id__in=item_id).update(show=True)
    else:
        return u'Error: not items'


    return aa, err1


print u"-= start =-"

import time
fal_name = "./../files/xml/admitad_products_20131215_161937.xml"

file_start = time.clock()
files = file_open(fal_name)
file_stop = time.clock()

cat_start = time.clock()
print admitad_category(fal_name)
cat_stop = time.clock()

item_start = time.clock()
print admitad_items(fal_name)
item_stop = time.clock()

print u"Файл: %s" % (file_stop - file_start)
print u"Категории: %s" % (cat_stop - cat_start)
print u"Товары: %s" % (item_stop - item_start)
print u"Всего: %s" % (item_stop - file_start)

print u"-= stop =-"