#-*- coding:utf-8 -*-
from django.contrib.sites.models import Site
from catalog.models import Category, Item


def site(request):
    return {
        'site': Site.objects.order_by('domain')[0:1].get(),
        'base_category': Category.objects.filter(show=True, level=1),
        'base_item': Item.objects.all().count()
    }