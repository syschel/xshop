# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'SubId.site_ref'
        db.add_column(u'statistic_subid', 'site_ref',
                      self.gf('django.db.models.fields.CharField')(max_length=500, null=True),
                      keep_default=False)

        # Adding field 'SubId.ip'
        db.add_column(u'statistic_subid', 'ip',
                      self.gf('django.db.models.fields.IPAddressField')(max_length=15, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'SubId.site_ref'
        db.delete_column(u'statistic_subid', 'site_ref')

        # Deleting field 'SubId.ip'
        db.delete_column(u'statistic_subid', 'ip')


    models = {
        u'statistic.subid': {
            'Meta': {'ordering': "('-create',)", 'object_name': 'SubId'},
            'create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'null': 'True'}),
            'item_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'item_name': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True'}),
            'item_url': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True'}),
            'keywords': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True'}),
            'referer': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True'}),
            'site_ref': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '1'}),
            'subid': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        }
    }

    complete_apps = ['statistic']