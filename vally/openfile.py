#-*- coding:utf-8 -*-
import sys, os, re
path = os.path.normpath(os.path.join(os.getcwd(), '..'))
sys.path.append(path)

import urllib2, cookielib
from random import choice


def url_open(url):
    """
        Функция открытия урла для парсинга
    """
    #try:
    user_agents = [
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11',
        'Opera/9.25 (Windows NT 5.1; U; en)',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
        'Mozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.5 (like Gecko) (Kubuntu)',
        'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070731 Ubuntu/dapper-security Firefox/1.5.0.12',
        'Lynx/2.8.5rel.1 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/1.2.9'
    ]
    headers = { 'User-Agent' : choice(user_agents) }
    values = {'name' : 'Michael Frankorf',
              'location' : 'Google LTD',
              'language' : 'en-US' }
    data = None #urllib.urlencode(values)
    req = urllib2.Request(url, data, headers)
    req.add_header('Referer', 'http://www.google.com/')
    req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
    req.add_header('Accept-Language', 'ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3')
    req.add_header('Content-Type', 'application/x-www-form-urlencoded')
    req.add_header('Connection', 'Keep-Alive')
    req.add_header('Accept-Charset', 'windows-1251,utf-8;q=0.7,*;q=0.7')

    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookielib.CookieJar()))
    urllib2.install_opener(opener)

    response = urllib2.urlopen(req)
    #file_object = open("thefile.html", 'w')
    #file_object.write(response.read())
    #file_object.close()
    return response.read()

    #except:
    #    print "fal"
    #    return False

def file_open(url, mode='r'):
    """
        Функция открытия файла для парсинга
    """

    response = open("./../files/" + url, mode)
    #response.write(response.read())
    #response.close()
    return response.read()