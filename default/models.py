#-*- coding:utf-8 -*-
from django.db import models

from tinymce.models import HTMLField

from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from functions.files import get_file_path


class Sites(Site):
    """ Профиль домена """

    meta            = models.TextField(verbose_name=u'Настройки meta-тегов', null=True, blank=True)
    robots          = models.TextField(verbose_name=u'Настройки robots.txt', null=True, blank=True)
    footer          = models.TextField(verbose_name=u'Вывод кода перед </body>', null=True, blank=True)
    title           = models.CharField(verbose_name=u'Заголовок главной', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'Ключевые главной', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'Описание главной', max_length=255, blank=True, null=True)
    about           = HTMLField(verbose_name=u'Описание главной', null=True, blank=True)

    objects = models.Manager()
    on_site = CurrentSiteManager()


    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = u'Сайты'
        verbose_name_plural = u'Сайты'

class UploadFiles(models.Model):
    """ Модель загрузки файлов """

    name            = models.CharField(verbose_name=u'Название файла', max_length=255)
    file            = models.FileField(verbose_name=u'Файл', upload_to=get_file_path, blank=True, null=True)
    created         = models.DateTimeField(verbose_name=u'Дата создания', auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.name


    class Meta:
        verbose_name = u'Загрузка файлов'
        verbose_name_plural = u'Загрузка файлов'

class SeoDefault(models.Model):
    """ Шаблоны СЕО """

    TEMPL0, TEMPL1, TEMPL2, TEMPL3, TEMPL4, TEMPL5, TEMPL6, TEMPL7, TEMPL8 = range(0, 9)
    TEMPLATE = (
        (TEMPL0, u'title_category'),
        (TEMPL1, u'keywords_category'),
        (TEMPL2, u'description_category'),
        (TEMPL3, u'title_item'),
        (TEMPL4, u'keywords_item'),
        (TEMPL5, u'description_item'),
        (TEMPL6, u'title_shops_list'),
        (TEMPL7, u'keywords_shops_list'),
        (TEMPL8, u'description_shops_list'),
    )

    text_seo        = models.CharField(verbose_name=u'Шаблон', max_length=255,
                                       help_text=u"__names__ - название объекта(не краткое),<br>"
                                                 u"__prices__ - цена товара,<br>"
                                                 u"__offers__ - оффер товара,<br>"
                                                 u"__categorys__ - категория товара")
    template_seo    = models.IntegerField(verbose_name=u'#Тип шаблона', max_length=11, default=TEMPL0, choices=TEMPLATE)
    created         = models.DateTimeField(verbose_name=u'Дата создания', auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.template_seo


    class Meta:
        verbose_name = u'Шаблоны СЕО'
        verbose_name_plural = u'Шаблоны СЕО'