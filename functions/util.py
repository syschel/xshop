# -*- coding: utf-8 -*-
import re
import sys
import time
import random
import string
import datetime
import calendar
import traceback

APP_DEFAULT_PERPAGE = 10

from django.core.paginator import Paginator, EmptyPage, InvalidPage


# Получить объекты для постраничного отображения
def get_paging_objects(request, object_list, item_pet_page):
    paginator = Paginator(object_list, item_pet_page)
    # Определим номер страницы
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # Если запрашиваю страницу больше 9999, то перейдем на последнюю
    try:
        objects = paginator.page(page)
    except (EmptyPage, InvalidPage):
        objects = paginator.page(paginator.num_pages)

    return objects


# Возвращает выбранное количество элементов на странице
def get_perpage_items(request):
    items_perpage = APP_DEFAULT_PERPAGE
    if request.COOKIES.has_key('items_perpage'):
        if request.COOKIES['items_perpage'] == 'all':
            items_perpage = sys.maxint
        else:
            items_perpage = int(request.COOKIES['items_perpage'])

    return items_perpage


# Конвертирует строку с датой в формате ISO (Y-m-d) в объект datetime.date
# Может выкидывать исключение ValueError
def iso_string_to_date(from_str):
    return datetime.date(*time.strptime(from_str, '%Y-%m-%d')[0:3])


# Конвертирует строку с датой в формате ISO (Y-m-d) в объект datetime.datetime
# Может выкидывать исключение ValueError
def iso_string_to_datetime(from_str):
    return datetime.datetime(*time.strptime(from_str, '%Y-%m-%d %H:%M:%S')[0:6])


# Формирование случайной строки из n символов
def get_random_string(n):
    return ''.join(random.choice(string.letters + string.digits) for x in range(n))


# Возвращает массив состоящий из значений всех полей key
# в двумерном массиве или в массиве объектов list
def list_of(list, key, objects=False, distinct=False):
    result_list = []
    for item in list:
        if objects:
            value = getattr(item, key)
        else:
            value = item[key]

        if distinct:
            if not value in result_list:
                result_list.append(value)
        else:
            result_list.append(value)

    return result_list


# Преобразует массив объектов, в масисв с ключем по полю key
def reindex_by(list, key, objects=False):
    result_dict = {}
    for item in list:
        if objects: k_value = getattr(item, key)
        else: k_value = item[key]
        result_dict[k_value] = item

    return result_dict


# Сгруппировать двумерный массив или массив объектов
# по полю с именем key
def group_by(list, key, objects=False):
    result_dict = {}
    for item in list:
        if objects: k_value = getattr(item, key)
        else: k_value = item[key]

        if not result_dict.has_key(k_value):
            result_dict[k_value] = []
        result_dict[k_value].append(item)

    return result_dict


# На основе даты "d" вернуть даты начала и конца месяца
def month_range(d, year=None):
    d_year = d.year
    if year: d_year = year

    month_wday, end_mday = calendar.monthrange(d_year, d.month)
    start_date = datetime.date(d_year, d.month, 1)
    end_date = datetime.date(d_year, d.month, end_mday)
    return start_date, end_date


# Список месяцев в году.
# Возвращается массив tuple,ов с
# номером месяца и объектами date (дата начала и дата конца)
def month_choices(year=None):
    choices = []
    d_year = datetime.date.today().year
    if year: d_year = year

    for i in range(1, 13):
        start_date = datetime.date(d_year, i, 1)
        month_wday, end_mday = calendar.monthrange(d_year, start_date.month)
        end_date = datetime.date(d_year, start_date.month, end_mday)
        choices.append((i, start_date, end_date))

    return choices

