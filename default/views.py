#-*- coding:utf-8 -*-

from django.shortcuts import render_to_response

from django.contrib.sites.models import Site


def robotstxt(request):
    # Генерируем robots.txt
    sites = Site.objects.get(domain=request.META['HTTP_HOST'])
    return render_to_response('robots.html', {'robots':sites}, mimetype="text/plain; charset=utf-8")

def werifyandex(request):
    # Генерируем robots.txt
    sites = {'sites':{'robots': '511a8c75b5d7'}}
    return render_to_response('robots.html', {'robots':sites}, mimetype="text/plain; charset=utf-8")