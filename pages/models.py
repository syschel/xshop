#-*- coding:utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from PIL import Image
from tinymce.models import HTMLField
import re
import unidecode
from functions.files import get_file_path


class Pages(models.Model):
    """
    Статичные страницы
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=False)
    images          = models.ImageField(verbose_name=u'Изображение', upload_to=get_file_path, blank=True, null=True)
    min_text        = models.TextField(verbose_name=u'Краткое описание', null=True, blank=True)
    text            = HTMLField(verbose_name=u'Подробное описание', null=True, blank=True)

    url             = models.SlugField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)
    title           = models.CharField(verbose_name=u'Заголовок окна', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'Ключевые', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'Описание', max_length=255, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('page', args=[self.url, self.pk])

    def save(self, size=(100, 100), *args, **kwargs):
        # Если нет титлы, воруем из названия категории
        if not self.title:
            self.title = self.name
        # Если нет ключевиков, воруем из названия категории
        if not self.keywords:
            self.keywords = self.name
        # Если нет описания, воруем из названия категории
        if not self.description:
            self.description = self.name
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.replace(" ", "_")))
        super(Pages, self).save(*args, **kwargs)

        if self.images:
            # Ресайзим картинку если она пришла
            # print u"-= Алярм, картынка ресайзится при каждом сохранении категории =-"
            filename = self.images.path
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename, image.format)

    class Meta:
        verbose_name = u'Статичные страницы'
        verbose_name_plural = u'Статичные страницы'
        ordering = ('-create',)