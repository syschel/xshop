#-*- coding:utf-8 -*-
from django.contrib import admin
from statistic.models import SubId


class SubIdAdmin(admin.ModelAdmin):
    list_display = ('subid', 'item_id', 'item_name', 'ip', 'status', 'create')
    list_display_links = ('subid',)
    search_fields = ['subid']
    list_filter = ('status',)
    date_hierarchy = 'create'


admin.site.register(SubId, SubIdAdmin)