#-*- coding:utf-8 -*-

from django.contrib import admin
from catalog.models import Category, Item, ItemOffer, ItemVendor, ItemPhoto, ItemParams, ItemParamsName, UpdateCSV,\
    SemanticText, SemanticReviews, SemanticTitle, FilterSetting, FilterSettingValues


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'short_name', 'parent', 'level', 'home', 'left', 'right', 'sort', 'show', 'create', 'update')
    list_display_links = ('id', 'name', 'short_name')
    list_filter = ('show', 'home', 'level',)
    search_fields = ['name', 'id']
    list_editable = ("sort",)
    raw_id_fields = ("parent",)
    readonly_fields = ('name',)

    fieldsets = (
        (u"Настройки", {'fields': ('parent', 'sort', 'show', 'home', 'url')}),
        (u"Общее", {'fields': ('name', 'short_name', 'h1', 'seo_text', 'images')}),
        (u"SEO", {'fields': ('title', 'keywords', 'description')}),
    )

class ItemPhotoInline(admin.TabularInline):
    model = ItemPhoto
    extra = 1

class ItemParamsInline(admin.TabularInline):
    model = ItemParams
    extra = 1

class ItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'oldprice', 'show', 'available', 'sort', 'create', 'update')
    list_display_links = ('name', 'price')
    list_filter = ('show', 'available', 'offer')
    search_fields = ['name']
    list_editable = ("sort",)
    raw_id_fields = ('category', 'vendor',)
    save_on_top = True

    fieldsets = (
        (u"Настройки", {'fields': ('category', 'offer', 'show', 'sort', 'ref_url')}),
        (u"Общее", {'fields': ('name', 'url', 'title', 'vendor', 'picture', 'thumbnail', 'price', 'oldprice', 'text', 'subtext')}),
        (u"XML", {'fields': ('xml_id', 'xml_original_id', 'xml_category_id')}),
    )

    inlines = [ItemPhotoInline, ItemParamsInline]

class ItemOfferAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'code', 'items', 'delivery', 'show', 'create', 'update')
    list_display_links = ('name', 'code')
    list_filter = ('show',)
    search_fields = ['name']
    ordering = ('-name',)
    save_on_top = True

    fieldsets = (
        (u"Настройки", {'fields': ('name', 'url', 'code', 'show', 'images')}),
        (u"Общее", {'fields': ('min_text', 'max_text', 'delivery', 'payment', 'return_policy', 'guarantee')}),
        (u"SEO", {'fields': ('title', 'keywords', 'description')}),
    )

class ItemVendorAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'items', 'show', 'create', 'update')
    list_display_links = ('name', 'code')
    list_filter = ('show',)
    search_fields = ['name']
    ordering = ('name',)

class ItemPhotoAdmin(admin.ModelAdmin):
    list_display = ('item', 'value')
    list_display_links = ('item', 'value')
    search_fields = ['item__name']
    raw_id_fields = ('item',)

class ItemParamsAdmin(admin.ModelAdmin):
    list_display = ('attr_id', 'value', 'sub_value')
    list_display_links = ('attr_id', 'value',)
    list_filter = ('attr',)
    raw_id_fields = ("item",)

    def attr_id(self, instance):
        return instance.attr.pk

class ItemParamsNameAdmin(admin.ModelAdmin):
    list_display = ('name', 'params_count', 'show')
    list_display_links = ('name',)
    list_filter = ('show',)
    ordering = ('name',)




class FilterSettingValuesInline(admin.TabularInline):
    model = FilterSettingValues
    extra = 1

class FilterSettingAdmin(admin.ModelAdmin):
    list_display = ('name_show', 'name', 'show',)
    list_display_links = ('name_show', 'name',)
    list_filter = ('show',)
    filter_horizontal = ('categoryes',)
    raw_id_fields = ('categoryes',)

    def cat_name(self, instance):
        return instance.categoryes.pk

    inlines = [FilterSettingValuesInline]





class SemanticAdmin(admin.ModelAdmin):
    raw_id_fields = ("category",)


class UpdateCSVAdmin(admin.ModelAdmin):
    list_display = ('name', 'show', 'photo', 'param', 'all_times', 'update', 'create')
    list_display_links = ('name',)
    list_filter = ('show',)
    ordering = ('update',)

admin.site.register(Category, CategoryAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(ItemOffer, ItemOfferAdmin)
admin.site.register(ItemVendor, ItemVendorAdmin)
admin.site.register(ItemPhoto, ItemPhotoAdmin)
admin.site.register(ItemParams, ItemParamsAdmin)
admin.site.register(ItemParamsName, ItemParamsNameAdmin)
admin.site.register(FilterSetting, FilterSettingAdmin)
admin.site.register(SemanticText, SemanticAdmin)
admin.site.register(SemanticReviews, SemanticAdmin)
admin.site.register(SemanticTitle, SemanticAdmin)
admin.site.register(UpdateCSV, UpdateCSVAdmin)
