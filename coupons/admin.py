#-*- coding:utf-8 -*-

from django.contrib import admin
from coupons.models import CuponADVCategory, CuponOffer, CuponAdvantagesOffer, CuponCategory, CuponTypes, CuponSpecies, CuponItem


class CuponADVCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent', 'sort', 'show', 'create', 'update')
    list_display_links = ('name', 'parent')
    list_filter = ('show',)
    search_fields = ['name']
    list_editable = ("sort",)

    fieldsets = (
        (u"Настройки", {'fields': ('parent', 'sort', 'show', 'url')}),
        (u"Общее", {'fields': ('name', 'h1', 'seo_text', 'images')}),
        (u"SEO", {'fields': ('title', 'keywords', 'description')}),
    )

class CuponAdvantagesOfferAdmin(admin.ModelAdmin):
    list_display = ('offer', 'name')
    list_display_links = ('offer', 'name')

class CuponAdvantagesOfferInline(admin.TabularInline):
    model = CuponAdvantagesOffer
    extra = 1

class CuponOfferAdmin(admin.ModelAdmin):
    list_display = ('name', 'site', 'xml_id', 'show', 'create', 'update')
    list_display_links = ('name', 'site')
    list_filter = ('show',)
    search_fields = ['name']
    inlines = [CuponAdvantagesOfferInline]




class CuponCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent', 'sort', 'show', 'create', 'update')
    list_display_links = ('name', 'parent')
    list_filter = ('show',)
    search_fields = ['name']
    list_editable = ("sort",)

    fieldsets = (
        (u"Настройки", {'fields': ('parent', 'sort', 'show', 'url')}),
        (u"Общее", {'fields': ('name', 'h1', 'seo_text', 'images')}),
        (u"SEO", {'fields': ('title', 'keywords', 'description')}),
    )

class CuponTypesAdmin(admin.ModelAdmin):
    list_display = ('name', 'xml_id')
    list_display_links = ('name', 'xml_id')

class CuponSpeciesAdmin(admin.ModelAdmin):
    list_display = ('name', 'xml_id')
    list_display_links = ('name', 'xml_id')

class CuponItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'short_name', 'xml_id', 'campany', 'specie', 'show', 'create', 'update')
    list_display_links = ('name', 'short_name', 'xml_id',)
    list_filter = ('show', 'specie',)
    search_fields = ['name', 'short_name']

admin.site.register(CuponADVCategory, CuponADVCategoryAdmin)
admin.site.register(CuponOffer, CuponOfferAdmin)
admin.site.register(CuponAdvantagesOffer, CuponAdvantagesOfferAdmin)
admin.site.register(CuponCategory, CuponCategoryAdmin)
admin.site.register(CuponTypes, CuponTypesAdmin)
admin.site.register(CuponSpecies, CuponSpeciesAdmin)
admin.site.register(CuponItem, CuponItemAdmin)


