# -*- coding: utf-8 -*-

from django.contrib import admin

from default.models import Sites, UploadFiles, SeoDefault
from django.contrib.sites.models import Site

class SitesAdmin(admin.ModelAdmin):
    list_display = ('domain', 'name',)
    list_display_links = ('name', 'domain')
    fieldsets = (
                (u'Настройки', {'fields': ('domain', 'name',)}),
                (u'Информация', {'fields': ('meta', 'footer', 'robots',)}),
                (u'SEO', {'fields': ('about', 'title', 'keywords', 'description',)}),
                 )
    #formfield_overrides = {models.TextField: {'widget': TinyMCE()}, }

class UploadFilesAdmin(admin.ModelAdmin):
    list_display = ('name', 'file', 'created',)
    list_display_links = ('name', 'file',)
    ordering = ('-created',)

class SeoDefaultAdmin(admin.ModelAdmin):
    list_display = ('template_seo', 'text_seo', 'created',)
    list_display_links = ('template_seo',)
    ordering = ('-created',)

admin.site.register(Sites, SitesAdmin)
admin.site.unregister(Site) # Уберём из админки дефолтовый, чтобы не пугать всех
admin.site.register(UploadFiles, UploadFilesAdmin)
admin.site.register(SeoDefault, SeoDefaultAdmin)


